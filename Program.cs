﻿namespace Olekstra.Util.BatchQueueSender
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Olekstra.Messaging;
    using Olekstra.Messaging.Messages;
    using Olekstra.Processing;

    public class Program
    {
        public static async Task Main(string[] args)
        {
            var config = new ConfigurationBuilder()
               .SetBasePath(AppContext.BaseDirectory)
               .AddJsonFile("appsettings.json", optional: false)
               .AddUserSecrets<Program>()
               .AddCommandLine(args)
               .Build();

            var sbcs = config["ServiceBusSenderOptions:ConnectionString"];
            var sbt = config["ServiceBusSenderOptions:Topic"];

            var services = new ServiceCollection()
                .AddLogging(o => o.SetMinimumLevel(LogLevel.Information).AddConsole())
                .AddSingleton<IConfiguration>(config)
                .AddServiceBusMessageSender(sbcs, sbt)
                .BuildServiceProvider();

            var logger = services.GetRequiredService<ILogger<Program>>();

            var sender = services.GetRequiredService<IMessageSender>();
            var counter = 0;

            for (var i = 683001425000; i <= 683001434999; i++)
            {
                if (!CardNumber.TryParse(i.ToString(), true, out var cn))
                {
                    throw new Exception("Ooops");
                }

                await sender.SendAsync(new CardUpdateRequest { Number = cn.ToString() });
                counter++;

                if (counter % 100 == 0)
                {
                    logger.LogInformation(cn.ToUserString());
                }
            }

            Console.WriteLine();
            Console.WriteLine("Done");
            Console.WriteLine("Press any key to quit");
            Console.ReadKey();
        }
    }
}
